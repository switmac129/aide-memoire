function getUserGreeting() {
    var greeting = "none";
    var timenow = Date.now();

    if (moment(timenow).format('a') === "am") {
        greeting = "Good Morning,";
    } else {
        greeting = "Good Evening,";
    }

    $('#user_greet').text(greeting);
}

function getRandomGreeting() {
    var greetings = ["How are you today?", "Care to share something?", "What's up doc?", "What are your thoughts?"];
    var randomGreeting = greetings[Math.floor(Math.random() * greetings.length)];

    $('#scribbletext').attr('placeholder', randomGreeting);
}

$(document).ready(function () {
    getUserGreeting();
    getRandomGreeting();
});