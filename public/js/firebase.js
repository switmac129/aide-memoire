    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyAuESzJMxCqqPqZn-iRX0TR6_oTpp0sf_0",
      authDomain: "aide-memoire-112517.firebaseapp.com",
      databaseURL: "https://aide-memoire-112517.firebaseio.com",
      projectId: "aide-memoire-112517",
      storageBucket: "aide-memoire-112517.appspot.com",
      messagingSenderId: "874412798462"
    };
    firebase.initializeApp(config);

    var stageUrl = "https://aide-memoire-112517.firebaseapp.com/";

    // DATABASE
    var dbRef = firebase.database();
    var entryRef = dbRef.ref('entry');
    var postRef = dbRef.ref('post');
    var photoRef = firebase.storage().ref();
    var uid = 0;


    $(document).ready(function () {

      firebase
        .auth()
        .onAuthStateChanged(function (user) {


          if (user) {
            uid = user.uid;

            var entry = firebase
              .database()
              .ref('entry/' + uid);
            entry.on('child_added', function (data) {
              console.log('refresh data', data);
              //appendCard('#post-area', post, ts);
            });


            $('#user_email').text(user.email);
            $('#user_name').text(user.displayName + "!");
            $('#user_photo').html("<img src='" + user.photoURL + "' height='100' width='100' style='border-radius: 100%;'>");

            // LOAD ALL ENTRIES
            dbRef
              .ref('entry/' + uid)
              .orderByValue()
              .once('value')
              .then(function (snap) {
                console.log(Object.keys(snap.val()).length);
                var posts = snap.val(),
                  postsKeys = Object.keys(snap.val());
                if (postsKeys.length) {
                  $('#post-area').html("");
                }

                //get random post
                console.log(postsKeys.length, 'random index', Math.floor(Math.random() * postsKeys.length));
                var randomPostKey = postsKeys[Math.floor(Math.random() * postsKeys.length)];
                dbRef
                  .ref('post/' + randomPostKey)
                  .once('value')
                  .then(function (snap) {
                    var post = snap.val();
                    console.log(snap.val(), posts[randomPostKey].timestamp);
                    appendCard('#featured-post', post, posts[randomPostKey].timestamp);
                  });


                snap.forEach(function (childSnapshot) {
                  console.log(childSnapshot.key, childSnapshot.val().timestamp);
                  var key = childSnapshot.key;
                  var ts = childSnapshot
                    .val()
                    .timestamp;

                  dbRef
                    .ref('post/' + key)
                    .once('value')
                    .then(function (snap) {
                      console.log(snap.val(), ts);
                      var post = snap.val();
                      appendCard('#post-area', post, ts);
                    });
                });
              });

          } else {
            // REDIRECT TO INDEX TO LOGIN
            window.location = stageUrl;
          }
        });
    });

    function appendCard(elem, post, ts) {
      var datetime = new Date(ts);

      switch (post.type) {
        case 'scribble':
          $(elem).append(
            "<div class='scribble post'>" +
            "<span class='post__content'>" + post.content + "</span>" +
            "<span class='post__date' data-placement='top' data-toggle='tooltip' title='" + datetime + "'>" + moment(new Date(ts), "YYYYMMDD").fromNow() + "</span>" +
            "</div>"
          );
          break;
        case 'quote':
          $(elem).append(
            "<div class='quote post'>" +
            "<span class='post__content'>" + post.content + "</span>" +
            " ― <span class='post__author'>" + post.author + "</span>, " +
            "<span class='post__reference'>" + post.reference + "</span>" +
            "<span class='post__date' data-placement='top' data-toggle='tooltip' title='" + datetime + "'>" + moment(new Date(ts), "YYYYMMDD").fromNow() + "</span>" +
            "</div>"
          );
          break;
        case 'photo':
          $(elem).append(
            "<div class='photo post'>" +
            "<span class='post__photo'><img width='400' height='300' src='" + post.url + "' /></span>" +
            "<span class='post__date' data-placement='top' data-toggle='tooltip' title='" + datetime + "'>" + moment(new Date(ts), "YYYYMMDD").fromNow() + "</span>" +
            "</div>"
          );
          break;
      }
    }

    function logout() {
      firebase.auth().signOut().then(function () {
        window.location = stageUrl;
      }).catch(function (error) {
        console.log(error);
      });
    }

    function addScribble() {
      var scribbletext = document.getElementById("scribbletext").value;
      var postid = postRef.push({
        content: scribbletext,
        type: "scribble"
      }).key;
      dbRef.ref('entry/' + uid + '/' + postid).set({
        timestamp: Date.now()
      });
    }

    function addQuote() {
      var quotetext = document.getElementById("quotetext").value;
      var authortext = document.getElementById("authortext").value;
      var reftext = document.getElementById("reftext").value;
      var postid = postRef.push({
        content: quotetext,
        author: authortext,
        reference: reftext,
        type: "quote"
      }).key;
      dbRef.ref('entry/' + uid + '/' + postid).set({
        timestamp: Date.now()
      });
    }

    function addPhoto() {
      console.log("enter upload");
      const file = $('#fileButton').get(0).files[0];
      const name = (+new Date()) + '-' + file.name;
      const metadata = {
        contentType: file.type
      };
      const task = photoRef.child(name).put(file, metadata);
      task.then((snapshot) => {
        var fileloc = snapshot.downloadURL;
        var postid = postRef.push({
          url: fileloc,
          type: "photo"
        }).key;
        dbRef.ref('entry/' + uid + '/' + postid).set({
          timestamp: Date.now()
        });
      });
    }